# Projeto de API Duocode

DuoCode é uma plataforma web de aprendizado de linguagens de programação inspirado na plataforma Duolingo. Mediante métodos de repetição, o aluno percorrerá uma árvore de tópicos, resolvendo sessões pequenas de perguntas relacionadas à linguagem, não só aprendendo novos conteúdos como revisando conteúdos de módulos passados.

[Acesse a wiki do projeto](https://gitlab.com/projeto-de-desenvolvimento/duo-code/api/-/wikis/home)

### Requisitos:

- Node
- MySQL
- NPM ou Yarn
- Git

### Passos para rodar o projeto:

**1 -** Variáveis de ambiente:
1. Copie o arquivo **.env-example** e renomeie para **.env**.
1. No .env, informe o **username** e **password** para se conectar com o banco mysql da sua máquina.
1. Será necessário também criar um banco com o nome "**duocode**".
1. Adicione uma palavra da sua prefência como chave secreta. Ela será responsável pela criptografia da senha de usuários no banco.

**2 -** Execute na raiz do projeto:
1. **npm install** ou **yarn install**
1. **npm run start:develop**
