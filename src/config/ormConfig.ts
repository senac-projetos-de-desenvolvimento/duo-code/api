import { ConnectionOptions } from "typeorm";

const ormConfig = (): ConnectionOptions => {
  return {
    type: "mysql",
    host: process.env.ORM_HOST,
    port: Number(process.env.ORM_PORT),
    username: process.env.ORM_USERNAME,
    password: process.env.ORM_PASSWORD,
    database: process.env.ORM_DATABASE,
    synchronize: Boolean(process.env.ORM_SYNCHRONIZE),
    logging: false,
    entities: ["src/entity/**/*.ts"],
    migrations: ["src/migration/**/*.ts"],
    subscribers: ["src/subscriber/**/*.ts"],
    cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber",
    },
  };
};

export default ormConfig;
