import { MigrationInterface, QueryRunner } from "typeorm";

export class AddLastSessionUser1599006370350 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE user ADD lastSession VARCHAR(60)`);
  }

  async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE user DROP COLUMN lastSession`);
  }
}
