import getUserByEmail from "../../repository/user/getUserByEmail";
import getProgress from "../../repository/courseProgress/getProgress";
import { getRepository } from "typeorm";
import { Certificate } from "../../entity/Certificate";
const jwt = require("jsonwebtoken");

const getCourseProgress = (router) => {
  return router.post("/progress", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = await payload.user.email;
    const user = await getUserByEmail(email);
    const courseId = await request.body.courseId;
    let courseProgress: any = await getProgress(user.id, courseId);

    if (courseProgress.finished) {
      const certificate = await getRepository(Certificate)
        .createQueryBuilder()
        .where("userId = :userId AND courseId = :courseId", {
          userId: user.id,
          courseId,
        })
        .getOne();

      courseProgress.hash = certificate.hash;
    }

    response.status(200).send(courseProgress);
  });
};

export default getCourseProgress;
