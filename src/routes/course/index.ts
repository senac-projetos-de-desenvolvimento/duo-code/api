const express = require("express");
const router = express.Router();

import getCourses from "./getCourses";
import getCourse from "./getCourse";
import addUserToCourse from "./addUserToCourse";
import getUserRegisteredCourses from "./getUserRegisteredCourses";
import getCourseModules from "./getCourseModules";
import getSessionQuestions from "./getSessionQuestions";
import getCourseProgress from "./getCourseProgress";
import sessionEnd from "./sessionEnd";

getCourses(router);
getCourse(router);
addUserToCourse(router);
getUserRegisteredCourses(router);
getCourseModules(router);
getSessionQuestions(router);
getCourseProgress(router);
sessionEnd(router);

module.exports = router;
