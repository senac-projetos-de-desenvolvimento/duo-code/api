import getQuestions from "../../repository/module/getQuestions";

const getSessionQuestions = (router) => {
  return router.post("/session", async (request, response) => {
    const moduleId = await request.body.moduleId;
    const questions = await getQuestions(moduleId);
    response.status(200).send(questions);
  });
};

export default getSessionQuestions;
