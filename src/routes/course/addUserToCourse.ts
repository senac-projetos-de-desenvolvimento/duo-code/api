import getUserByEmail from "../../repository/user/getUserByEmail";
import getCourseById from "../../repository/course/getCourseById";
import newStudent from "../../repository/courseProgress/newStudent";
import getCourseProgress from "../../repository/courseProgress/getProgress";
const jwt = require("jsonwebtoken");

const addUserToCourse = (router) => {
  return router.post("/user/new", async (request, response) => {
    const courseId = request.body.courseId;
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;
    let error = false;
    try {
      const course = await getCourseById(courseId);
      if (course) {
        const user = await getUserByEmail(email);
        if (user) {
          const userIsNotStudent = await userNotStudent(course, user);
          if (userIsNotStudent) {
            await newStudent(user, course)
              .then(() => {
                response.status(200).send("Você entrou no curso!");
              })
              .catch((error) => {
                response.status(500);
              });
            return;
          }
        }
      }
      response.status(500).send("Ocorreu um problema na requisição");
    } catch (error) {
      console.log(error);
      response.status(500);
    }
  });
};

const userNotStudent = async (course, user) => {
  const userInCourse: any = await getCourseProgress(user.id, course.id);
  if (userInCourse) {
    return false;
  } else {
    return true;
  }
};

export default addUserToCourse;
