import getUserByEmail from "../../repository/user/getUserByEmail";
import getUserCourses from "../../repository/courseProgress/getUserCourses";
const jwt = require("jsonwebtoken");

const getUserRegisteredCourses = async (router) => {
  return router.get("/user/courses", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;
    try {
      const user = await getUserByEmail(email);
      const courses = await getUserCourses(user.id);
      if (courses) {
        response.status(200).send(courses);
      } else {
        response.status(500);
      }
    } catch (err) {
      console.log(err);
      response.status(500);
    }
  });
};

export default getUserRegisteredCourses;
