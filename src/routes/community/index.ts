const express = require("express");
const router = express.Router();

import searchAsking from "./searchAsking";
import getSpotlightAskings from "./getSpotlightAskings";

getSpotlightAskings(router);
searchAsking(router);

module.exports = router;
