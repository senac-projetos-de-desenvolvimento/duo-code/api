import getSpotlightAskings from "../../repository/community/getSpotlightAskings";
const jwt = require("jsonwebtoken");

const spotlightAskings = (router) => {
  return router.get("/spotlightAskings/:page", async (request, response) => {
    const page = request.params.page;
    const data = await getSpotlightAskings(page);

    if (data) {
      response.status(200).send(data);
      return;
    }

    response.status(404).send();
  });
};

export default spotlightAskings;
