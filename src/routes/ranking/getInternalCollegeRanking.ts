import { getRepository } from "typeorm";
import getUser from "../../../common/getUser";
import { Institution } from "../../entity/Institution";
import { User } from "../../entity/User";

const getCollegeRanking = async (router) => {
  return router.get("/college/:page", async (request, response) => {
    const user = await getUser(request);
    const collegeId = user.institutionId;
    const page = request.params.page - 1;
    const min = page * 10;
    const max = min + 10;

    if (!collegeId) {
      response.status(404).send("Não encontrado.");
      return;
    }

    const ranking = await getRepository(Institution)
      .createQueryBuilder("institution")
      .where("institution.id = :id", { id: collegeId })
      .leftJoinAndSelect("institution.users", "users")
      .leftJoinAndSelect("users.courseProgress", "courseProgress")
      .orderBy("courseProgress.totalXp", "DESC")
      .skip(min)
      .take(max)
      .getMany();

    const collegeUsers = [];

    await Promise.all(
      ranking[0].users.map(async (user) => {
        let totalXp = 0;

        await Promise.all(
          user.courseProgress.map((course) => {
            totalXp += course.totalXp;
          })
        );

        collegeUsers.push({
          user_id: user.id,
          username: user.username,
          totalXp,
          perfilImage: user.perfilImage,
          gender: user.gender,
          offensive: user.offensive,
        });
      })
    );

    response.status(200).send(collegeUsers);

    // const users = await getRepository(User).createQueryBuilder("user");

    // const ranking = await getRepository(Institution)
    //   .createQueryBuilder("institution")
    //   .where("institution.id = :collegeId", { collegeId })
    //   .leftJoinAndSelect((subQuery) => {
    //     return subQuery
    //       .from(User, "user")
    //       .innerJoin("user.courseProgress", "courseProgress");
    //   }, "user")
    //   .orderBy("user.totalXp", "DESC")
    //   .skip(min)
    //   .take(max)
    //   .getMany();
  });
};

export default getCollegeRanking;
