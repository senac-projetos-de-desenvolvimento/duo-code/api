const express = require("express");
const router = express.Router();

import getCollegeRanking from "./getCollegesRanking";
import getInternalCollegeRanking from "./getInternalCollegeRanking";

getCollegeRanking(router);
getInternalCollegeRanking(router);

module.exports = router;
