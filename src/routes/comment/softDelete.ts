import getUserByEmail from "../../repository/user/getUserByEmail";
import deleteComment from "../../repository/comment/delete";
const jwt = require("jsonwebtoken");

const deleteCommentById = (router) => {
  return router.delete("/:id", async (request, response) => {
    const commentId = request.params.id;

    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;

    const user = await getUserByEmail(email);

    if (user) {
      deleteComment(commentId)
        .then(() => {
          response.status(200).send("Comentário deletado com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao deletar o comentário.");
        });
    } else {
      response.status(500).send("Erro de chave de autenticação.");
    }
  });
};

export default deleteCommentById;
