import getComment from "../../repository/comment/get";

const getCommentById = (router) => {
  return router.get("/:id", async (request, response) => {
    const id = request.params.id;

    if (id) {
      const comment = await getComment(id);
      response.status(200).send(comment);
    } else {
      response.status(500).send();
    }
  });
};

export default getCommentById;
