import updateComment from "../../repository/comment/update";
import getComment from "../../repository/comment/get";

const insertComment = (router) => {
  return router.put("/:id", async (request, response) => {
    const commentId = request.params.id;
    const comment: any = await getComment(commentId);

    comment.content = request.body.content || comment.content;

    if (comment) {
      updateComment(comment)
        .then(() => {
          response.status(200).send("Comentário atualizado com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao atualizar o comentário.");
        });
    } else {
      response.status(500).send("Comentário não encontrado.");
    }
  });
};

export default insertComment;
