import getAsking from "../../repository/asking/getAsking";
const jwt = require("jsonwebtoken");

const getAsk = (router) => {
  return router.get("/:id", async (request, response) => {
    const id = request.params.id;

    if (id) {
      const asking = await getAsking(id);
      response.status(200).send(asking);
    } else {
      response.status(500).send();
    }
  });
};

export default getAsk;
