import updateAsking from "../../repository/asking/updateAsking";
import getAsking from "../../repository/asking/getAsking";

const insertQuestion = (router) => {
  return router.put("/:id", async (request, response) => {
    const asking: any = await getAsking(request.params.id);

    asking.title = request.body.title || asking.title;
    asking.content = request.body.content || asking.content;
    asking.tags = request.body.tags || asking.tags;
    asking.active = request.body.active || asking.active;

    if (asking) {
      updateAsking(asking)
        .then(() => {
          response.status(200).send("Pergunta atualizada com sucesso.");
        })
        .catch(() => {
          response.status(500).send("Erro ao atualizar a pergunta.");
        });
    } else {
      response.status(500).send("Pergunta não encontrada.");
    }
  });
};

export default insertQuestion;
