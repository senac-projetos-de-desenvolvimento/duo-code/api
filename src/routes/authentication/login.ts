const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

import { getRepository, getConnection, Connection } from "typeorm";
import { User } from "../../entity/User";

const login = async (request, response) => {
  const user = {
    email: request.body.email,
    password: request.body.password,
  };

  try {
    const userDB = await getRepository(User)
      .createQueryBuilder("user")
      .select("user.email")
      .where("user.email = :email", { email: user.email })
      .addSelect("user.password")
      .getOne();

    if (userDB) {
      bcrypt.compare(user.password, userDB["password"], async (err, result) => {
        if (!err) {
          if (result) {
            jwt.sign(
              { user },
              process.env.SECRET_KEY,
              { expiresIn: "60h" },
              (err, token) => {
                if (err) {
                  response.status(401).send();
                } else {
                  response.status(200).json({
                    token,
                  });
                }
              }
            );
          } else {
            response.status(401).send("Erro ao criar o token.");
          }
        } else {
          response.status(401).send("Erro no resultado do token.");
          throw err;
        }
      });
    } else {
      response.status(401).send("Usuário não identificado.");
    }
  } catch (err) {
    response.status(401).send("Erro interno de autenticação.");
  }
};

export default login;
