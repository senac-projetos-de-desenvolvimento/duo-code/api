import getCertificate from "./getCertificate";

const express = require("express");
const router = express.Router();

getCertificate(router);

module.exports = router;
