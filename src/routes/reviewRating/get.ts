import getReviewRatingById from "../../repository/reviewRating/get";

const getReviewRating = (router) => {
  return router.get("/:id", async (request, response) => {
    const id = request.params.id;

    if (id) {
      const comment = await getReviewRatingById(id);
      response.status(200).send(comment);
    } else {
      response.status(500).send();
    }
  });
};

export default getReviewRating;
