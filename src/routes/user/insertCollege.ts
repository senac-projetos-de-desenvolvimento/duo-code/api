import getUserByEmail from "../../repository/user/getUserByEmail";
import insertCollege from "../../repository/user/insertCollege";
const jwt = require("jsonwebtoken");

const getUserStats = async (router) => {
  return router.post("/insert/college", async (request, response) => {
    const [, token] = request.headers.authorization.split(" ");
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    const email = payload.user.email;
    const user = await getUserByEmail(email);

    const collegeId = request.body.collegeId;

    await insertCollege(user.id, collegeId)
      .then(() => {
        response.status(200).send();
      })
      .catch((error) => {
        response.status(500).send();
      });
  });
};

export default getUserStats;
