import { getRepository } from "typeorm";
import { ReviewRating } from "../../entity/ReviewRating";

const deleteReviewRating = async (data) => {
  await getRepository(ReviewRating)
    .createQueryBuilder()
    .update(ReviewRating)
    .set({ like: data.like, user: data.user, comment: data.comment })
    .where("id = :id", { id: data.id })
    .execute();
};

export default deleteReviewRating;
