import { getRepository } from "typeorm";
import { ReviewRating } from "../../entity/ReviewRating";

const deleteReviewRating = async (id) => {
  await getRepository(ReviewRating)
    .createQueryBuilder()
    .delete()
    .where("id = :id", { id })
    .execute();
};

export default deleteReviewRating;
