import { getRepository } from "typeorm";
import { Comment } from "../../entity/Comment";

const deleteComment = async (data) => {
  await getRepository(Comment)
    .createQueryBuilder()
    .update(Comment)
    .set({ content: data.content })
    .where("id = :id", { id: data.id })
    .execute();
};

export default deleteComment;
