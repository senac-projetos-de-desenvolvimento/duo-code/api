import { getRepository } from "typeorm";
import { Comment } from "../../entity/Comment";

const deleteComment = async (userId) => {
  await getRepository(Comment)
    .createQueryBuilder("comment")
    .select()
    .leftJoinAndSelect("comment.user", "user")
    .where("user.id = :id", { id: userId })
    .getMany();
};

export default deleteComment;
