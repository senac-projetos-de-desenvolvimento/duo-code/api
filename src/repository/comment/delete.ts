import { getRepository } from "typeorm";
import { Comment } from "../../entity/Comment";

const deleteComment = async (id) => {
  const comment = await getRepository(Comment)
    .createQueryBuilder()
    .update(Comment)
    .set({ active: false })
    .where("id = :id", { id })
    .execute();
};

export default deleteComment;
