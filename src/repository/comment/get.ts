import { getRepository } from "typeorm";
import { Comment } from "../../entity/Comment";

const getComment = async (id) => {
  const comment: any = await getRepository(Comment)
    .createQueryBuilder("comment")
    .where("comment.id = :id", { id })
    .leftJoinAndSelect("comment.user", "user")
    .leftJoinAndSelect("comment.asking", "asking")
    .leftJoinAndSelect("comment.reviewRatings", "reviewRatings")
    .getOne();

  comment.like = comment.reviewRatings.filter((r) => r.like).length;
  comment.dislike = comment.reviewRatings.length - comment.like;
  comment.reviewRatings = undefined;

  return comment;
};

export default getComment;
