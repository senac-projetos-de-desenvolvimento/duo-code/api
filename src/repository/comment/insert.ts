import { getRepository } from "typeorm";
import { Comment } from "../../entity/Comment";

const insertComment = async (content, asking, user) => {
  await getRepository(Comment)
    .createQueryBuilder()
    .insert()
    .into(Comment)
    .values({
      content,
      asking,
      user,
    })
    .execute();
};

export default insertComment;
