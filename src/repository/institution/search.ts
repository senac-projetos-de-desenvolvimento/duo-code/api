import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";

const search = async (query) => {
  const institution = await getRepository(Institution)
    .createQueryBuilder("institution")
    .where("institution.name like :query", { query: `%${query}%` })
    .getMany();

  return institution;
};

export default search;
