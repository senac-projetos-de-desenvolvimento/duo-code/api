import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";

const getAllInstitution = async () => {
  const institution = await getRepository(Institution)
    .createQueryBuilder()
    .getMany();
  return institution;
};

export default getAllInstitution;
