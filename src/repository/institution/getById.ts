import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";

const getById = async (id) => {
  const institution = await getRepository(Institution)
    .createQueryBuilder("institution")
    .where("institution.id = :id", { id })
    .getOne();

  return institution;
};

export default getById;
