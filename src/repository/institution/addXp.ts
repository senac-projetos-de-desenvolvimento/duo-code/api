import { getRepository } from "typeorm";
import { Institution } from "../../entity/Institution";
import getInstitutionById from "./getById";

const updateLastSessionTimeAndOffensive = async (institutionId, xp) => {
  const institution = await getInstitutionById(institutionId);

  return await getRepository(Institution)
    .createQueryBuilder()
    .update(Institution)
    .set({
      totalXp: Number(institution.totalXp) + Number(xp),
    })
    .where("id = :id", { id: institutionId })
    .execute();
};

export default updateLastSessionTimeAndOffensive;
