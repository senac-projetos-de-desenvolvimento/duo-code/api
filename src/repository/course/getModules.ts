import { getRepository } from "typeorm";
import { Module } from "../../entity/Module";

const getModules = async (courseId) => {
  const modules = await getRepository(Module)
    .createQueryBuilder("module")
    .where("courseId = :id", { id: courseId })
    .orderBy("level")
    .getMany();

  return modules;
};

export default getModules;
