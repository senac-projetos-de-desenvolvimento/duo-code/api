import { getRepository } from "typeorm";
import { User } from "../../entity/User";

const getUserByEmail = async (email) => {
  const user = await getRepository(User)
    .createQueryBuilder("user")
    .where("email = :email", { email })
    .leftJoinAndSelect("user.institution", "institution")
    .leftJoinAndSelect("user.courseProgress", "courseProgress")
    .leftJoinAndSelect("courseProgress.course", "course")
    .getOne();
  return user;
};

export default getUserByEmail;
