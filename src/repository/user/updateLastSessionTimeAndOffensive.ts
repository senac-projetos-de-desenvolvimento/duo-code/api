import { getRepository } from "typeorm";
import { User } from "../../entity/User";

const updateLastSessionTimeAndOffensive = async (
  userId,
  offensive,
  userLastSession
) => {
  if (!offensive) {
    offensive = 1;
  } else if (isYesterday(userLastSession)) {
    offensive = offensive + 1;
  }

  await getRepository(User)
    .createQueryBuilder()
    .update(User)
    .set({
      lastSession: getTime(),
      offensive,
    })
    .where("id = :id", { id: userId })
    .execute();
};

const getTime = () => {
  return new Date() + "";
};

const isYesterday = (date: string) => {
  const actual = new Date().toLocaleDateString();
  let userLastSession = new Date(date);
  userLastSession.setDate(userLastSession.getDate() + 1);

  return actual === userLastSession.toLocaleDateString() ? true : false;
};

export default updateLastSessionTimeAndOffensive;
