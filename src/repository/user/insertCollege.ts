import { getRepository } from "typeorm";
import { User } from "../../entity/User";
import getCollegeById from "../institution/getById";

const insertCollege = async (userId, collegeId) => {
  const institution = await getCollegeById(collegeId);

  return await getRepository(User)
    .createQueryBuilder()
    .update(User)
    .set({
      institution,
    })
    .where("id = :id", { id: userId })
    .execute();
};

export default insertCollege;
