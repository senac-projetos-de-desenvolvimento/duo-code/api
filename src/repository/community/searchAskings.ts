import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";

const searchAsking = async (query) => {
  let askings: any = await getRepository(Asking)
    .createQueryBuilder("asking")
    .select()
    .where("asking.content like :query OR asking.tags like :query", {
      query: `%${query}%`,
    })
    .orderBy("asking.createdAt", "DESC")
    .leftJoinAndSelect("asking.comments", "comments")
    .take(8)
    .getMany();

  await askings.map((ask) => {
    ask.number_comments = ask.comments.length;
    ask.comments = undefined;
  });

  return { askings };
};

export default searchAsking;
