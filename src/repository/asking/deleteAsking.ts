import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";
import { User } from "../../entity/User";

const deleteAsking = async (id) => {
  const asking = await getRepository(Asking)
    .createQueryBuilder()
    .update(Asking)
    .set({ active: false })
    .where("id = :id", { id })
    .execute();
};

export default deleteAsking;
