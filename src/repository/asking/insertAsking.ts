import { getRepository } from "typeorm";
import { Asking } from "../../entity/Asking";
import { User } from "../../entity/User";

const insertAsking = async (title, content, tags, user) => {
  await getRepository(Asking)
    .createQueryBuilder()
    .insert()
    .into(Asking)
    .values({
      title,
      content,
      tags,
      user,
    })
    .execute();
};

export default insertAsking;
