import { getRepository } from "typeorm";
import { Module } from "../../entity/Module";

const getModuleById = async (moduleId) => {
  const module = await getRepository(Module)
    .createQueryBuilder()
    .where("id = :id", { id: moduleId })
    .getOne();

  return module;
};

export default getModuleById;
