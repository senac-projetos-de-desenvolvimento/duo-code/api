import { getRepository } from "typeorm";
import { CourseProgress } from "../../entity/CourseProgress";

const newStudent = async (user, course) => {
  await getRepository(CourseProgress)
    .createQueryBuilder()
    .insert()
    .into(CourseProgress)
    .values({
      totalXp: 0,
      level: 1,
      user,
      course,
    })
    .execute();
};

export default newStudent;
