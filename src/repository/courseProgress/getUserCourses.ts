import { getRepository } from "typeorm";
import { CourseProgress } from "../../entity/CourseProgress";

const getUserCourses = async (userId) => {
  const userCourses = await getRepository(CourseProgress)
    .createQueryBuilder("CourseProgress")
    .select(["CourseProgress", "course"])
    .where("userId = :id", { id: userId })
    .leftJoin("CourseProgress.course", "course")
    .getMany();

  return userCourses;
};

export default getUserCourses;
