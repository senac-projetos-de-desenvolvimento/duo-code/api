import { getConnection } from "typeorm";
import { CourseProgress } from "../../entity/CourseProgress";

const updateFinishedCourseProgress = async (
  userId,
  courseId,
  finished: boolean
) => {
  await getConnection()
    .createQueryBuilder()
    .update(CourseProgress)
    .set({
      finished,
    })
    .where("courseId = :courseId AND userId = :userId", {
      userId,
      courseId,
    })
    .execute();
};

export default updateFinishedCourseProgress;
