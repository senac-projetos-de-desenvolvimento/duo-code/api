import { getRepository, getConnection, Connection } from "typeorm";
import { Module } from "../../../../../entity/Module";
import { Question } from "../../../../../entity/Question";

const introductionQuestions = async (module) => {
  await insertQuestion(
    module,
    "Introdução - Javascript",
    "A JavaScript é uma linguagem dinâmica orientada a objetos (...)",
    true,
    "...",
    "...",
    0
  );
  await insertQuestion(
    module,
    "-",
    "JavaScript é uma linguagem dinâmica orientada a objetos.",
    true,
    "JavaScript é uma linguagem orientada a objetos!",
    "true",
    2
  );
  await insertQuestion(
    module,
    "-",
    " A JavaScript foi criada em...",
    true,
    "...",
    "1995*1917*2005*2018",
    1
  );
  await insertQuestion(
    module,
    "-",
    "Um tipo que NÃO é aceito no JavaScript padrão",
    true,
    "...",
    "Float*Const*Let*Var",
    1
  );
  await insertQuestion(
    module,
    "-",
    "Você pode declarar uma variável como Float.",
    true,
    "Javascript não tem um tipo chamado Float",
    "false",
    2
  );
  await insertQuestion(
    module,
    "-",
    "Javascrip e Java são a mesma linguagem.",
    true,
    "...",
    "false",
    2
  );
  await insertQuestion(
    module,
    "-",
    "Corpo da pergunta: 7",
    true,
    "...",
    "Sim*Não*Mais ou menos*Teste",
    1
  );
  await insertQuestion(
    module,
    "-",
    "Corpo da pergunta: 8",
    true,
    "...",
    "Sim*Não*Mais ou menos*Teste",
    1
  );
};

const insertQuestion = async (
  module: Module,
  title: string,
  questionText: string,
  primary: boolean,
  tip: string,
  answer: string,
  format: number
) => {
  try {
    await getRepository(Question)
      .createQueryBuilder()
      .insert()
      .into(Question)
      .values({
        title,
        questionText,
        primary,
        tip,
        module,
        answer,
        format,
      })
      .execute();
  } catch (err) {
    console.log(err);
  }
};

export default introductionQuestions;
