import { getRepository, getConnection, Connection } from "typeorm";
import { Course } from '../../../../entity/Course'
import { Module } from '../../../../entity/Module'
import introductionQuestions from './Questions/introductionQuestions'

const introduction = async (course) => {
    await getRepository(Module).createQueryBuilder()
        .insert()
        .values({
            name: "Funções I",
            moduleImage: "https://i.ibb.co/HF8kYsF/automation.png",
            level: 2,
            course,
        })
        .execute()
    let module = await getRepository(Module).createQueryBuilder().where("name = 'Funções I'").getOne()
    introductionQuestions(module)
}

export default introduction