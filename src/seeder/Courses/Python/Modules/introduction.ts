import { getRepository, getConnection, Connection } from "typeorm";
import { Course } from '../../../../entity/Course'
import { Module } from '../../../../entity/Module'
import introductionQuestions from './Questions/introductionQuestions'

const introduction = async (course) => {
    await getRepository(Module).createQueryBuilder()
        .insert()
        .values({
            name: "Introdução",
            moduleImage: "https://i.ibb.co/NtVh7q4/javascript-module-introduction.png",
            level: 1,
            course,
        })
        .execute()
    let module = await getRepository(Module).createQueryBuilder().where("name = 'Introdução'").getOne()
    introductionQuestions(module)
}

export default introduction