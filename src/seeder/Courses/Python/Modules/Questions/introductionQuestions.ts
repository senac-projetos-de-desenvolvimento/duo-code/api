import { getRepository, getConnection, Connection } from "typeorm";
import { Module } from '../../../../../entity/Module'
import { Question } from '../../../../../entity/Question'

const introductionQuestions = async (module) => {
    await insertQuestion(module, "Introdução - Java", "Bem vindo ao curso javascript", true, "...", "...", 0)
    await insertQuestion(module, "Segunda Questão", "Corpo da pergunta: 2", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Terceira Questão", "Corpo da pergunta: 3", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Quarta Questão", "Corpo da pergunta: 4", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Quinta Questão", "Corpo da pergunta: 5", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Sexta Questão", "Corpo da pergunta: 6", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Sétima Questão", "Corpo da pergunta: 7", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
    await insertQuestion(module, "Oitava Questão", "Corpo da pergunta: 8", true, "...", "Sim*Não*Mais ou menos*Teste", 1)
}

const insertQuestion = async (
    module: Module,
    title: string,
    questionText: string,
    primary: boolean,
    tip: string,
    answer: string,
    format: number,
) => {
    await getRepository(Question).createQueryBuilder()
        .insert()
        .into(Question)
        .values({
            title,
            questionText,
            primary,
            tip,
            module,
            answer,
            format,
        })
        .execute()
}

export default introductionQuestions