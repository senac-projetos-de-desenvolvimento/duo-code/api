import { getRepository, getConnection, Connection } from "typeorm";
import { Course } from '../../../entity/Course'
import introduction from './Modules/introduction'
import variable1 from './Modules/variable1'
import functions from './Modules/functions'
import variable2 from './Modules/variable2'

const pythonCourse = async () => {
    await getRepository(Course).createQueryBuilder()
        .insert()
        .into(Course)
        .values({
            name: "Python",
            version: "0.1",
            logoImage: "https://i.ibb.co/SRkLG8D/python-logo.png",
        })
        .execute()
    const course = await getRepository(Course).createQueryBuilder().where("name = 'Python'").getOne()
    // await introduction(course)
    // await variable1(course)
    // await functions(course)
    // await variable2(course)
}

export default pythonCourse