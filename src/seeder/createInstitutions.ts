import { getRepository } from "typeorm";
import { Institution } from "../entity/Institution";

export default async function createInstitutions() {
  createInstitution(
    "Pelotas",
    "RS",
    "Faculdade de Tecnologia Senac Pelotas",
    685
  );
  createInstitution("Pelotas", "RS", "Universidade Federal de Pelotas", 85);
  createInstitution("Pelotas", "RS", "Uninter", 10);
  createInstitution("Pelotas", "RS", "Universidade Católica de Pelotas", 10);
  createInstitution("Pelotas", "RS", "Universidade Unopar", 5);
  createInstitution(
    "Rio Grande",
    "RS",
    "Universidade Federal do Rio Grande",
    234
  );
}

const createInstitution = (city, state, name, totalXp) => {
  getRepository(Institution)
    .createQueryBuilder()
    .insert()
    .into(Institution)
    .values({
      city,
      name,
      state,
      totalXp,
    })
    .execute();
};
