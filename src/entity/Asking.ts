import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Comment } from "./Comment";
import { User } from "./User";

@Entity()
export class Asking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 5000 })
  content: string;

  @Column("varchar", { length: 100 })
  title: string;

  @Column("varchar", { length: 255 })
  tags: string;

  @Column("boolean", { default: false })
  resolved: boolean;

  @Column("boolean", { default: true })
  active: boolean;

  @ManyToOne((type) => User, (user) => user.askings, { nullable: false })
  user: User;

  @OneToMany((type) => Comment, (comment) => comment.asking, { nullable: true })
  comments: Comment[];

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
