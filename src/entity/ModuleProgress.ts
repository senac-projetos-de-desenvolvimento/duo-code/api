import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { CourseProgress } from "./CourseProgress";
import { Module } from "./Module";

@Entity()
export class ModuleProgress {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne((type) => Module, (module) => module.moduleProgress)
  module: Module;

  @ManyToOne(
    (type) => CourseProgress,
    (courseProgress) => courseProgress.moduleProgress
  )
  courseProgress: CourseProgress;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
