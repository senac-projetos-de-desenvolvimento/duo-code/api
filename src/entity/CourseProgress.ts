import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { Course } from "./Course";
import { ModuleProgress } from "./ModuleProgress";

@Entity()
export class CourseProgress {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("int")
  totalXp: number;

  @Column("int")
  level: number;

  @Column("bool", { default: false })
  finished: boolean;

  @ManyToOne((type) => User, (user) => user.courseProgress)
  user: User;

  @ManyToOne((type) => Course, (course) => course.courseProgress)
  course: Course;

  @OneToMany(
    (type) => ModuleProgress,
    (moduleProgress) => moduleProgress.courseProgress
  )
  moduleProgress: ModuleProgress[];

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
