import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Comment } from "./Comment";
import { Asking } from "./Asking";
import { CourseProgress } from "./CourseProgress";
import { Institution } from "./Institution";
import { ReviewRating } from "./ReviewRating";
import { Certificate } from "./Certificate";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 40 })
  username: string;

  @Column("varchar", { length: 80 })
  email: string;

  @Column("varchar", { length: 255, select: false })
  password: string;

  @Column("varchar", { length: 255, nullable: true })
  perfilImage: string;

  @Column("datetime", { nullable: true, select: false })
  dateBirth: string;

  @Column("tinyint", { nullable: true })
  gender: number;

  @Column("int", { nullable: true })
  offensive: number;

  @Column("varchar", { length: 255, nullable: true })
  lastSession: string;

  @OneToMany(
    (type) => CourseProgress,
    (course_progress) => course_progress.user
  )
  courseProgress: CourseProgress[];

  @ManyToOne((type) => Institution, (institution) => institution.users, {
    nullable: true,
  })
  institution: Institution;

  @Column({ nullable: true })
  institutionId: string;

  @OneToMany((type) => Asking, (asking) => asking.user, { nullable: true })
  askings: Asking[];

  @OneToMany((type) => Comment, (comment) => comment.user, { nullable: true })
  comments: Comment[];

  @OneToMany((type) => Certificate, (certificate) => certificate.user, {
    nullable: true,
  })
  certificates: Certificate[];

  @OneToMany((type) => ReviewRating, (reviewRating) => reviewRating.comment)
  reviewRatings: ReviewRating;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
