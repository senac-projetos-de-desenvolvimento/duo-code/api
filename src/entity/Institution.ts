import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./User";

@Entity()
export class Institution {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 80 })
  name: string;

  @Column("varchar", { length: 60 })
  city: string;

  @Column("varchar", { length: 60 })
  state: string;

  @Column("bigint")
  totalXp: number;

  @OneToMany((type) => User, (user) => user.institution, {
    nullable: true,
  })
  users: User[];

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
