import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Asking } from "./Asking";
import { User } from "./User";
import { ReviewRating } from "./ReviewRating";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 40 })
  content: string;

  @Column("boolean", { default: true })
  active: boolean;

  @ManyToOne((type) => Asking, (asking) => asking.comments, { nullable: false })
  asking: Asking;

  @ManyToOne((type) => User, (user) => user.comments, { nullable: false })
  user: User;

  @OneToMany((type) => ReviewRating, (reviewRating) => reviewRating.comment)
  reviewRatings: ReviewRating;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
