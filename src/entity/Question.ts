import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { Module } from "./Module";

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", { length: 45 })
  title: string;

  @Column("varchar", { length: 5000 })
  questionText: string;

  @Column("boolean")
  primary: boolean;

  @Column("varchar", { length: 255 })
  tip: string;

  @Column("varchar", { length: 255 })
  answer: string;

  @Column("tinyint")
  format: number;

  @ManyToOne((type) => Module, (module) => module.questions)
  module: Module;

  @CreateDateColumn({ name: "created_at" })
  createdAt!: Date;

  @UpdateDateColumn({ name: "updated_at" })
  UpdatedAt!: Date;
}
